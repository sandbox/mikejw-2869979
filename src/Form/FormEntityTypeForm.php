<?php

namespace Drupal\form_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FormEntityTypeForm.
 *
 * @package Drupal\form_entity\Form
 */
class FormEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $form_entity_type->label(),
      '#description' => $this->t("Label for the Form entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $form_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\form_entity\Entity\FormEntityType::load',
      ],
      '#disabled' => !$form_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_entity_type = $this->entity;
    $status = $form_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Form entity type.', [
          '%label' => $form_entity_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Form entity type.', [
          '%label' => $form_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($form_entity_type->toUrl('collection'));
  }

}
