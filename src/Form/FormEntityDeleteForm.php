<?php

namespace Drupal\form_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Form entity entities.
 *
 * @ingroup form_entity
 */
class FormEntityDeleteForm extends ContentEntityDeleteForm {


}
