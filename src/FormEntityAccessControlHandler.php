<?php

namespace Drupal\form_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Form entity entity.
 *
 * @see \Drupal\form_entity\Entity\FormEntity.
 */
class FormEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\form_entity\Entity\FormEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished form entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published form entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit form entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete form entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add form entity entities');
  }

}
