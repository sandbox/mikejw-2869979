<?php

namespace Drupal\form_entity\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\form_entity\Entity\FormEntityInterface;

/**
 * Class FormEntityController.
 *
 *  Returns responses for Form entity routes.
 *
 * @package Drupal\form_entity\Controller
 */
class FormEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Form entity  revision.
   *
   * @param int $form_entity_revision
   *   The Form entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($form_entity_revision) {
    $form_entity = $this->entityManager()->getStorage('form_entity')->loadRevision($form_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('form_entity');

    return $view_builder->view($form_entity);
  }

  /**
   * Page title callback for a Form entity  revision.
   *
   * @param int $form_entity_revision
   *   The Form entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($form_entity_revision) {
    $form_entity = $this->entityManager()->getStorage('form_entity')->loadRevision($form_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $form_entity->label(), '%date' => format_date($form_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Form entity .
   *
   * @param \Drupal\form_entity\Entity\FormEntityInterface $form_entity
   *   A Form entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(FormEntityInterface $form_entity) {
    $account = $this->currentUser();
    $langcode = $form_entity->language()->getId();
    $langname = $form_entity->language()->getName();
    $languages = $form_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $form_entity_storage = $this->entityManager()->getStorage('form_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $form_entity->label()]) : $this->t('Revisions for %title', ['%title' => $form_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all form entity revisions") || $account->hasPermission('administer form entity entities')));
    $delete_permission = (($account->hasPermission("delete all form entity revisions") || $account->hasPermission('administer form entity entities')));

    $rows = [];

    $vids = $form_entity_storage->revisionIds($form_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\form_entity\FormEntityInterface $revision */
      $revision = $form_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $form_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.form_entity.revision', ['form_entity' => $form_entity->id(), 'form_entity_revision' => $vid]));
        }
        else {
          $link = $form_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.form_entity.translation_revert', ['form_entity' => $form_entity->id(), 'form_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.form_entity.revision_revert', ['form_entity' => $form_entity->id(), 'form_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.form_entity.revision_delete', ['form_entity' => $form_entity->id(), 'form_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['form_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
