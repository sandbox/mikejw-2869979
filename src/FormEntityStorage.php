<?php

namespace Drupal\form_entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\form_entity\Entity\FormEntityInterface;

/**
 * Defines the storage handler class for Form entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Form entity entities.
 *
 * @ingroup form_entity
 */
class FormEntityStorage extends SqlContentEntityStorage implements FormEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(FormEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {form_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {form_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(FormEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {form_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('form_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
