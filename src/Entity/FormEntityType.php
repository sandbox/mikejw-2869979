<?php

namespace Drupal\form_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Form entity type entity.
 *
 * @ConfigEntityType(
 *   id = "form_entity_type",
 *   label = @Translation("Form entity type"),
 *   handlers = {
 *     "list_builder" = "Drupal\form_entity\FormEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\form_entity\Form\FormEntityTypeForm",
 *       "edit" = "Drupal\form_entity\Form\FormEntityTypeForm",
 *       "delete" = "Drupal\form_entity\Form\FormEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\form_entity\FormEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "form_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "form_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/form_entity_type/{form_entity_type}",
 *     "add-form" = "/admin/structure/form_entity_type/add",
 *     "edit-form" = "/admin/structure/form_entity_type/{form_entity_type}/edit",
 *     "delete-form" = "/admin/structure/form_entity_type/{form_entity_type}/delete",
 *     "collection" = "/admin/structure/form_entity_type"
 *   }
 * )
 */
class FormEntityType extends ConfigEntityBundleBase implements FormEntityTypeInterface {

  /**
   * The Form entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Form entity type label.
   *
   * @var string
   */
  protected $label;

}
