<?php

namespace Drupal\form_entity\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Form entity entities.
 *
 * @ingroup form_entity
 */
interface FormEntityInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Form entity name.
   *
   * @return string
   *   Name of the Form entity.
   */
  public function getName();

  /**
   * Sets the Form entity name.
   *
   * @param string $name
   *   The Form entity name.
   *
   * @return \Drupal\form_entity\Entity\FormEntityInterface
   *   The called Form entity entity.
   */
  public function setName($name);

  /**
   * Gets the Form entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Form entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Form entity creation timestamp.
   *
   * @param int $timestamp
   *   The Form entity creation timestamp.
   *
   * @return \Drupal\form_entity\Entity\FormEntityInterface
   *   The called Form entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Form entity published status indicator.
   *
   * Unpublished Form entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Form entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Form entity.
   *
   * @param bool $published
   *   TRUE to set this Form entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\form_entity\Entity\FormEntityInterface
   *   The called Form entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Form entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Form entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\form_entity\Entity\FormEntityInterface
   *   The called Form entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Form entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Form entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\form_entity\Entity\FormEntityInterface
   *   The called Form entity entity.
   */
  public function setRevisionUserId($uid);

}
