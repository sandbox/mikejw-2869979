<?php

namespace Drupal\form_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Form entity type entities.
 */
interface FormEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
