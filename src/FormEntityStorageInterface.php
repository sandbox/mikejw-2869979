<?php

namespace Drupal\form_entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\form_entity\Entity\FormEntityInterface;

/**
 * Defines the storage handler class for Form entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Form entity entities.
 *
 * @ingroup form_entity
 */
interface FormEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Form entity revision IDs for a specific Form entity.
   *
   * @param \Drupal\form_entity\Entity\FormEntityInterface $entity
   *   The Form entity entity.
   *
   * @return int[]
   *   Form entity revision IDs (in ascending order).
   */
  public function revisionIds(FormEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Form entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Form entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\form_entity\Entity\FormEntityInterface $entity
   *   The Form entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(FormEntityInterface $entity);

  /**
   * Unsets the language for all Form entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
